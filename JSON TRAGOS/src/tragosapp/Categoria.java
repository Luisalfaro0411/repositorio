/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tragosapp;

import javafx.scene.image.Image;
import org.json.JSONObject;

/**
 *
 * @author Mañana_pos5
 */
public class Categoria {
    String id;
    String categoria;
    String imagen;

    public Categoria(String id, String categoria, String imagen) {
        this.id = id;
        this.categoria = categoria;
        this.imagen = imagen;
    }

    

    

    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

   

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    
    

    
    
}
