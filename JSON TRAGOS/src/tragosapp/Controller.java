package tragosapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.json.JSONArray;
import org.json.JSONObject;
import javafx.scene.input.MouseEvent;

public class Controller implements Initializable {

    @FXML
    private Button btnGlass;

    @FXML
    private Button btnRandom;

    @FXML
    private Button btnTipo;

    @FXML
    private ComboBox cbTipo;

    @FXML
    private Button btnCategorias;

    @FXML
    private ComboBox cbCategoria;

    @FXML
    private ListView<Categoria> cbList;

    @FXML
    private TableView<Categoria> tvBebidas;

    @FXML
    private ComboBox cbGlass;

    @FXML
    private ImageView imgHola1;

    @FXML
    private Label txt1;

    @FXML
    private Label txt2;

    @FXML
    private Label txt4;

    @FXML
    private TableColumn colID;

    @FXML
    private TableColumn colNombre;
    
    @FXML
    private TableColumn colImagen;

    @FXML
    public ObservableList<Categoria> bebidas;

    ObservableList<String> listaCategorias;
    ObservableList<Categoria> listaCategorias1;
    ObservableList<String> listaCategorias2;
    ObservableList<Categoria> listaCategorias3;
    ObservableList<String> listaCategorias4;
    ObservableList<Categoria> listaCategorias5;
    ObservableList<Categoria> listaCategorias6;
    ObservableList<String> listaCategorias7;

    private void tragosCategorias() throws IOException {
        listaCategorias = FXCollections.observableArrayList();
        String url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list";
        InputStream inputStream;
        inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String response;
        response = bufferedReader.readLine();
        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("drinks");
        JSONObject categoria;
        for (int i = 0; i < jsonArray.length(); i++) {
            categoria = jsonArray.getJSONObject(i);
            listaCategorias.add(categoria.getString("strCategory"));
        }
        cbCategoria.setItems(listaCategorias);

    }

    @FXML
    void cargarTabla1(ActionEvent event) {
        bebidas = FXCollections.observableArrayList();
        if (cbCategoria.getSelectionModel().getSelectedIndex() > -1) {
            String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=" + cbCategoria.getSelectionModel().getSelectedItem();
            InputStream inputStream = null;
            try {
                inputStream = new URL(url).openStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String response = null;
            try {
                response = bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("drinks");
            JSONObject categoria;
            for (int i = 0; i < jsonArray.length(); i++) {
                categoria = jsonArray.getJSONObject(i);
                String chiste = categoria.getString("idDrink");
                String chiste1 = categoria.getString("strDrink");
                String chiste2 = categoria.getString("strDrinkThumb");
                bebidas.add(new Categoria(chiste, chiste1,chiste2));

            }
            tvBebidas.setItems(bebidas);

            
        } else {
            System.out.println("No hay categoria seleccionada");
        }
    }

    private void tragosTipo() throws IOException {
        listaCategorias2 = FXCollections.observableArrayList();
        String url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?a=list";
        InputStream inputStream;
        inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String response;
        response = bufferedReader.readLine();
        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("drinks");
        JSONObject categoria;
        for (int i = 0; i < jsonArray.length(); i++) {
            categoria = jsonArray.getJSONObject(i);
            listaCategorias2.add(categoria.getString("strAlcoholic"));
        }
        cbTipo.setItems(listaCategorias2);

    }

    private void tragosGlass() throws IOException {
        listaCategorias4 = FXCollections.observableArrayList();
        String url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?g=list";
        InputStream inputStream;
        inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String response;
        response = bufferedReader.readLine();
        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("drinks");
        JSONObject categoria;
        for (int i = 0; i < jsonArray.length(); i++) {
            categoria = jsonArray.getJSONObject(i);
            listaCategorias4.add(categoria.getString("strGlass"));
        }
        cbGlass.setItems(listaCategorias4);

    }

    @FXML
    void cargarTabla2(ActionEvent event) {
        listaCategorias3 = FXCollections.observableArrayList();
        if (cbTipo.getSelectionModel().getSelectedIndex() > -1) {
            String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=" + cbTipo.getSelectionModel().getSelectedItem();
            InputStream inputStream = null;
            try {
                inputStream = new URL(url).openStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String response = null;
            try {
                response = bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("drinks");
            JSONObject categoria;
            for (int i = 0; i < jsonArray.length(); i++) {
                categoria = jsonArray.getJSONObject(i);
                String chiste = categoria.getString("idDrink");
                String chiste1 = categoria.getString("strDrink");
                String chiste2 = categoria.getString("strDrinkThumb");
                listaCategorias3.add(new Categoria(chiste, chiste1,chiste2));

            }
            tvBebidas.setItems(listaCategorias3);
            

        } else {
            System.out.println("No hay categoria seleccionada");
        }
    }

    @FXML
    void cargarTabla3(ActionEvent event) {
        listaCategorias5 = FXCollections.observableArrayList();
        if (cbGlass.getSelectionModel().getSelectedIndex() > -1) {
            String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?g=" + cbGlass.getSelectionModel().getSelectedItem();
            InputStream inputStream = null;
            try {
                inputStream = new URL(url).openStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String response = null;
            try {
                response = bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("drinks");
            JSONObject categoria;
            for (int i = 0; i < jsonArray.length(); i++) {
                categoria = jsonArray.getJSONObject(i);
                String chiste = categoria.getString("idDrink");
                String chiste1 = categoria.getString("strDrink");
                String chiste2 = categoria.getString("strDrinkThumb");
                listaCategorias5.add(new Categoria(chiste, chiste1,chiste2));

            }
            tvBebidas.setItems(listaCategorias5);
            

        } else {
            System.out.println("No hay categoria seleccionada");
        }
    }

    @FXML
    void cargartabla4(ActionEvent event) throws IOException {
        listaCategorias6 = FXCollections.observableArrayList();
        String url = "https://www.thecocktaildb.com/api/json/v1/1/random.php";
        InputStream inputStream;
        inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String response;
        response = bufferedReader.readLine();
        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("drinks");
        JSONObject categoria;
        for (int i = 0; i < jsonArray.length(); i++) {
            categoria = jsonArray.getJSONObject(i);
            String chiste = categoria.getString("idDrink");
            String chiste1 = categoria.getString("strDrink");
                String chiste2 = categoria.getString("strDrinkThumb");
                listaCategorias6.add(new Categoria(chiste, chiste1,chiste2));
            txt1.setText(categoria.getString("idDrink"));
            txt2.setText(categoria.getString("strDrink"));
            imgHola1.setImage(new Image(categoria.getString("strDrinkThumb")));
        }

        tvBebidas.setItems(listaCategorias6);
    }

  
    @FXML
    void mostrarDatos(MouseEvent event) {
        Categoria bb=tvBebidas.getSelectionModel().getSelectedItem();
        Image bebida=new Image(bb.getImagen());
        imgHola1.setImage(bebida);
        txt1.setText(bb.getId());
        txt2.setText(bb.getCategoria());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.colID.setCellValueFactory(new PropertyValueFactory("id"));
        this.colNombre.setCellValueFactory(new PropertyValueFactory("categoria"));
        this.colImagen.setCellValueFactory(new PropertyValueFactory("imagen"));
        try {
            tragosCategorias();
            tragosTipo();
            tragosGlass();
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
