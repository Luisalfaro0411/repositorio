/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videojuego;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import static videojuego.CombateController.delay;

public class PersonajeSeleccionadoController implements Initializable{

    @FXML
    private Button btn1;
    
    @FXML
    public ImageView img1;

    @FXML
    public ImageView img2;

    @FXML
    public ImageView img3;
    
    @FXML
    public Label txt1;

    @FXML
    public Label txt2;
    
    @FXML
    public Label txt3;
    
    @FXML
    void empezarBatalla(ActionEvent event) {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("Combate.fxml"));
            Scene scene = new Scene(fxmlLoader.load(),1096,652);
            Stage stage = new Stage();stage.setTitle("");
            stage.setScene(scene);stage.show();
            Stage myStage = (Stage) this.btn1.getScene().getWindow();
            myStage.close();
            CombateController combateController = (CombateController) fxmlLoader.getController();
             if(txt1.getText().equals("GOKU")){
                        
                        combateController.fondot.setImage(new Image("Imagenes/gokuderecha.gif"));
                        combateController.fondot.setFitWidth(225);
                        combateController.fondot.setFitHeight(316);
                        combateController.labelE.setText("GOKU");
                        
                    }
                    if(txt1.getText().equals("TRUNKS")){
                        
                        combateController.fondot.setImage(new Image("Imagenes/trunksderecha.gif"));
                        combateController.fondot.setFitWidth(225);
                        combateController.fondot.setFitHeight(316);
                        combateController.labelE.setText("TRUNKS");
                    }
                    if(txt1.getText().equals("VEGETA")){
                        combateController.fondot.setImage(new Image("Imagenes/vegetaderecha.gif"));
                        combateController.fondot.setFitWidth(225);
                        combateController.fondot.setFitHeight(316);
                        combateController.labelE.setText("VEGETA");
                    }
                    if(txt1.getText().equals("CELL")){
                        combateController.fondot.setImage(new Image("Imagenes/cellderecha.gif"));
                        combateController.fondot.setFitWidth(225);
                        combateController.fondot.setFitHeight(316);
                        combateController.labelE.setText("CELL");
                        
                    }
                    if(txt1.getText().equals("KID BUU")){
                        combateController.fondot.setImage(new Image("Imagenes/buuderecha.gif"));
                        combateController.fondot.setFitWidth(225);
                        combateController.fondot.setFitHeight(316);
                        combateController.labelE.setText("KID BUU");
                    }
                    if(txt1.getText().equals("GOHAN")){
                        combateController.fondot.setImage(new Image("Imagenes/gohanderecha.gif"));
                        combateController.fondot.setFitWidth(225);
                        combateController.fondot.setFitHeight(316);
                        combateController.labelE.setText("GOHAN");
                    }
                    //--------------------------------------------------------------------------------
                    if(txt2.getText().equals("GOKU")){
                        combateController.fondot2.setImage(new Image("Imagenes/gokuizquierda.gif"));
                        combateController.fondot2.setFitWidth(225);
                        combateController.fondot2.setFitHeight(316);
                        combateController.labelE1.setText("GOKU");
                    }
                    if(txt2.getText().equals("TRUNKS")){
                        combateController.fondot2.setImage(new Image("Imagenes/trunksizquierda.gif"));
                        combateController.fondot2.setFitWidth(225);
                        combateController.fondot2.setFitHeight(316);
                        combateController.labelE1.setText("TRUNKS");
                    }
                    if(txt2.getText().equals("VEGETA")){
                        combateController.fondot2.setImage(new Image("Imagenes/vegetaizquierda.gif"));
                        combateController.fondot2.setFitWidth(225);
                        combateController.fondot2.setFitHeight(316);
                        combateController.labelE1.setText("VEGETA");
                    }
                    if(txt2.getText().equals("CELL")){
                        combateController.fondot2.setImage(new Image("Imagenes/cellizquierda.gif"));
                        combateController.fondot2.setFitWidth(225);
                        combateController.fondot2.setFitHeight(316);
                        combateController.labelE1.setText("CELL");
                    }
                    if(txt2.getText().equals("KID BUU")){
                        combateController.fondot2.setImage(new Image("Imagenes/buuizquierda.gif"));
                        combateController.fondot2.setFitWidth(225);
                        combateController.fondot2.setFitHeight(316);
                        combateController.labelE1.setText("KID BUU");
                    }
                    if(txt2.getText().equals("GOHAN")){
                        combateController.fondot2.setImage(new Image("Imagenes/gohanizquierda.gif"));
                        combateController.fondot2.setFitWidth(225);
                        combateController.fondot2.setFitHeight(316);
                        combateController.labelE1.setText("GOHAN");
                    }
                    if(txt3.getText().equals("CAMPO1")){
                        combateController.fondo1.setImage(new Image("Imagenes/fondocombate4.jpg"));
                        combateController.fondo1.setFitWidth(1096);
                        combateController.fondo1.setFitHeight(652);
                    }
                    if(txt3.getText().equals("CAMPO2")){
                        combateController.fondo1.setImage(new Image("Imagenes/fondocombate3.jpg"));
                        combateController.fondo1.setFitWidth(1096);
                        combateController.fondo1.setFitHeight(652);
                    }
                    if(txt3.getText().equals("CAMPO3")){
                        combateController.fondo1.setImage(new Image("Imagenes/fondocombate2.jpg"));
                        combateController.fondo1.setFitWidth(1096);
                        combateController.fondo1.setFitHeight(652);
                    }
            
           
            }catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
            
        }
    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btn1.setVisible(false);
        delay(4000, () -> btn1.fire());
        txt1.setVisible(false);
        txt2.setVisible(false);
        txt3.setVisible(false);
        
    }
    
    public static void delay(long millis, Runnable continuation) {
        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> continuation.run());
        new Thread(sleeper).start();
    }

}

