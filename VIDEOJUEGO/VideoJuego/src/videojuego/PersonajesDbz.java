package videojuego;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.Blend;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;


public class PersonajesDbz implements Initializable{

    
    
    @FXML
    private Label texto1;
    
    @FXML
    private Button btnResultados;
    
     @FXML
    private Label texto2;
    
    @FXML
    private Button btnEmpezar;

    @FXML
    private Button btnLimpiar;

    @FXML
    private Button btnLimpiar2;

    @FXML
    private ImageView img1;

    @FXML
    private ImageView img10;

    @FXML
    private ImageView img11;

    @FXML
    private ImageView img12;

    @FXML
    private ImageView img13;

    @FXML
    private ImageView img14;

    @FXML
    private ImageView img15;

    @FXML
    private ImageView img2;

    @FXML
    private ImageView img3;

    @FXML
    private ImageView img4;

    @FXML
    private ImageView img5;

    @FXML
    private ImageView img6;

    @FXML
    private ImageView img7;

    @FXML
    private ImageView img8;

    @FXML
    private ImageView img9;

    @FXML
    private ImageView imgFlecha;

    @FXML
    private ImageView imgc1;

    @FXML
    private ImageView imgc2;

    @FXML
    private ImageView imgc3;

    @FXML
    private ImageView imgc4;

    @FXML
    public Label label1;

    @FXML
    public Label label2;

    @FXML
    private Label label4;

    @FXML
    private Label label6;

    private int seleccion;
    
    @FXML
    void cargarResultados(ActionEvent event) {
        try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("cuadroResultado.fxml"));
                Scene scene = new Scene(fxmlLoader.load(), 711  , 412);
                Stage stage = new Stage();
                stage.setTitle("");
                stage.setScene(scene);
                stage.show();
                Stage myStage = (Stage) this.btnResultados.getScene().getWindow();
                myStage.close();


        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }
    
    @FXML
    void cargarimagen1(MouseEvent event) {
        if(event.getSource().equals(img3)){
            
            Image im1 = new Image("Imagenes/gokuCompleto.png");
            img15.setImage(im1);img15.setFitHeight(300);
            img15.setFitWidth(300);label1.setText("GOKU");
        }
        else if(event.getSource().equals(img4)){
            Image im1 = new Image("Imagenes/trunksCompleto.png");
            img15.setImage(im1);img15.setFitHeight(300);
            img15.setFitWidth(300);label1.setText("TRUNKS");
        }
        
        else if(event.getSource().equals(img5)){
           Image im1 = new Image("Imagenes/vegetaCompleto.png");
            img15.setImage(im1);img15.setFitHeight(300);
            img15.setFitWidth(300);label1.setText("VEGETA");
        }
        else if(event.getSource().equals(img6)){
            Image im1 = new Image("Imagenes/cellCompleto.png");
            img15.setImage(im1);img15.setFitHeight(300);img15.setFitWidth(300);
            label1.setText("CELL");
        }
        else if(event.getSource().equals(img7)){
            Image im1 = new Image("Imagenes/buuCompleto.png");
            img15.setImage(im1);img15.setFitHeight(300);
            img15.setFitWidth(300);label1.setText("KID BUU");
        }
        else if(event.getSource().equals(img8)){
            Image im1 = new Image("Imagenes/gohanCompleto.png");
            img15.setImage(im1);img15.setFitHeight(300);
            img15.setFitWidth(300);label1.setText("GOHAN");
        }
        
    }

    @FXML
    void cargarimagen2(MouseEvent event) {
        if(event.getSource().equals(img9)){
            Image im1 = new Image("Imagenes/gokuCompleto.png");
            img2.setImage(im1);img2.setFitHeight(300);
            img2.setFitWidth(300);label2.setText("GOKU");
        }
        else if(event.getSource().equals(img10)){
            Image im1 = new Image("Imagenes/trunksCompleto.png");
            img2.setImage(im1);img2.setFitHeight(300);
            img2.setFitWidth(300);label2.setText("TRUNKS");
        }
        else if(event.getSource().equals(img11)){
           Image im1 = new Image("Imagenes/vegetaCompleto.png");
            img2.setImage(im1);img2.setFitHeight(300);
            img2.setFitWidth(300);label2.setText("VEGETA");
        }
        else if(event.getSource().equals(img12)){
            Image im1 = new Image("Imagenes/cellCompleto.png");
            img2.setImage(im1);img2.setFitHeight(300);
            img2.setFitWidth(300);label2.setText("CELL");
        }
        else if(event.getSource().equals(img13)){
            Image im1 = new Image("Imagenes/buuCompleto.png");
            img2.setImage(im1);img2.setFitHeight(300);
            img2.setFitWidth(300);label2.setText("KID BUU");
        }
        else if(event.getSource().equals(img14)){
            Image im1 = new Image("Imagenes/gohanCompleto.png");
            img2.setImage(im1);img2.setFitHeight(300);
            img2.setFitWidth(300);label2.setText("GOHAN");
        }
    }

    @FXML
    void cargarimagen3(MouseEvent event) {
        if(event.getSource().equals(imgc1)){
            Image im1 = new Image("Imagenes/fondocombate4.jpg");
            imgc4.setImage(im1);imgc4.setFitHeight(200);imgc4.setFitWidth(400);
        }
        else if(event.getSource().equals(imgc2)){
            Image im1 = new Image("Imagenes/fondocombate3.jpg");
            imgc4.setImage(im1);imgc4.setFitHeight(200);imgc4.setFitWidth(400);
        }
        else if(event.getSource().equals(imgc3)){
            Image im1 = new Image("Imagenes/fondocombate2.jpg");
            imgc4.setImage(im1);imgc4.setFitHeight(200);imgc4.setFitWidth(400);
        }
    }

    

    @FXML
    void limpiar2(ActionEvent event) {
            Blend b=new Blend();
            img9.setEffect(b);img10.setEffect(b);imgc1.setEffect(b);imgc2.setEffect(b);imgc3.setEffect(b);
            img11.setEffect(b);img12.setEffect(b);img13.setEffect(b);img14.setEffect(b);
            imgc1.setEffect(b);imgc2.setEffect(b);imgc3.setEffect(b);
            img9.setDisable(false);img10.setDisable(false);img11.setDisable(false);
            img12.setDisable(false);img13.setDisable(false);img14.setDisable(false);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
    }

    @FXML
    void limpiarTodo(ActionEvent event) {
            Blend b=new Blend();
            img3.setEffect(b);img4.setEffect(b);img5.setEffect(b);img6.setEffect(b);
            img7.setEffect(b);img8.setEffect(b);imgc1.setEffect(b);imgc2.setEffect(b);imgc3.setEffect(b);
            img3.setDisable(false);img4.setDisable(false);img5.setDisable(false);
            img6.setDisable(false);img7.setDisable(false);img8.setDisable(false);
            img9.setDisable(true);img10.setDisable(true);img11.setDisable(true);
            img12.setDisable(true);img13.setDisable(true);img14.setDisable(true);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
    }

    @FXML
    void presionar1(MouseEvent event) {
        if(event.getSource().equals(img3)){
            Image im1 = new Image("Imagenes/gokuCompleto.png");
            Lighting lighting = new Lighting();
            
            img3.setEffect(lighting);img15.setImage(im1);img15.setFitHeight(320);img15.setFitWidth(400);
            img3.setDisable(true);img4.setDisable(true);img5.setDisable(true);
            img6.setDisable(true);img7.setDisable(true);img8.setDisable(true);
            img9.setDisable(false);img10.setDisable(false);img11.setDisable(false);
            img12.setDisable(false);img13.setDisable(false);img14.setDisable(false);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
            btnLimpiar.setDisable(false);
        }
        else if(event.getSource().equals(img4)){
            Image im1 = new Image("Imagenes/trunksCompleto.png");
            Lighting lighting = new Lighting();
            img4.setEffect(lighting);img15.setImage(im1);img15.setFitHeight(320);img15.setFitWidth(400);
            img3.setDisable(true);img4.setDisable(true);img5.setDisable(true);
            img6.setDisable(true);img7.setDisable(true);img8.setDisable(true);
            img9.setDisable(false);img10.setDisable(false);img11.setDisable(false);
            img12.setDisable(false);img13.setDisable(false);img14.setDisable(false);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
            btnLimpiar.setDisable(false);
        }
        else if(event.getSource().equals(img5)){
            Image im1 = new Image("Imagenes/vegetaCompleto.png");
            Lighting lighting = new Lighting();
            img5.setEffect(lighting);img15.setImage(im1);img15.setFitHeight(320);img15.setFitWidth(400);
            img3.setDisable(true);img4.setDisable(true);img5.setDisable(true);
            img6.setDisable(true);img7.setDisable(true);img8.setDisable(true);
            img9.setDisable(false);img10.setDisable(false);img11.setDisable(false);
            img12.setDisable(false);img13.setDisable(false);img14.setDisable(false);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
            btnLimpiar.setDisable(false);
        }
        else if(event.getSource().equals(img6)){
            Image im1 = new Image("Imagenes/cellCompleto.png");
            Lighting lighting = new Lighting();
            img6.setEffect(lighting);img15.setImage(im1);img15.setFitHeight(320);img15.setFitWidth(400);
            img3.setDisable(true);img4.setDisable(true);img5.setDisable(true);
            img6.setDisable(true);img7.setDisable(true);img8.setDisable(true);
            img9.setDisable(false);img10.setDisable(false);img11.setDisable(false);
            img12.setDisable(false);img13.setDisable(false);img14.setDisable(false);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
            btnLimpiar.setDisable(false);
        }
        else if(event.getSource().equals(img7)){
            Image im1 = new Image("Imagenes/buuCompleto.png");
            Lighting lighting = new Lighting();
            img7.setEffect(lighting);img15.setImage(im1);img15.setFitHeight(320);img15.setFitWidth(400);
            img3.setDisable(true);img4.setDisable(true);img5.setDisable(true);
            img6.setDisable(true);img7.setDisable(true);img8.setDisable(true);
            img9.setDisable(false);img10.setDisable(false);img11.setDisable(false);
            img12.setDisable(false);img13.setDisable(false);img14.setDisable(false);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false); 
            btnLimpiar.setDisable(false);
        }
        else  if(event.getSource().equals(img8)){
            Image im1 = new Image("Imagenes/gohanCompleto.png");
            Lighting lighting = new Lighting();
            img8.setEffect(lighting);img15.setImage(im1);img15.setFitHeight(320);img15.setFitWidth(400);
            img3.setDisable(true);img4.setDisable(true);img5.setDisable(true);
            img6.setDisable(true);img7.setDisable(true);img8.setDisable(true);
            img9.setDisable(false);img10.setDisable(false);img11.setDisable(false);
            img12.setDisable(false);img13.setDisable(false);img14.setDisable(false);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);  
            btnLimpiar.setDisable(false);
        }
        
    }

    @FXML
    void presionar2(MouseEvent event) {
            if(event.getSource().equals(img9)){
            Image im1 = new Image("Imagenes/gokuCompleto.png");
            Lighting lighting = new Lighting();
            img9.setEffect(lighting);img2.setImage(im1);img2.setFitHeight(320);img2.setFitWidth(400);
            img9.setDisable(true);img10.setDisable(true);img11.setDisable(true);
            img12.setDisable(true);img13.setDisable(true);img14.setDisable(true);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
            btnLimpiar2.setDisable(false);
    }
            else if(event.getSource().equals(img10)){
            Image im1 = new Image("Imagenes/trunksCompleto.png");
            Lighting lighting = new Lighting();
            img10.setEffect(lighting);img2.setImage(im1);img2.setFitHeight(320);img2.setFitWidth(400);
            img9.setDisable(true);img10.setDisable(true);img11.setDisable(true);
            img12.setDisable(true);img13.setDisable(true);img14.setDisable(true);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
            btnLimpiar2.setDisable(false);}
            
            else if(event.getSource().equals(img11)){
            Image im1 = new Image("Imagenes/vegetaCompleto.png");
            Lighting lighting = new Lighting();
            img11.setEffect(lighting);img2.setImage(im1);img2.setFitHeight(320);img2.setFitWidth(400);
            img9.setDisable(true);img10.setDisable(true);img11.setDisable(true);
            img12.setDisable(true);img13.setDisable(true);img14.setDisable(true);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
            btnLimpiar2.setDisable(false);
    }
            else if(event.getSource().equals(img12)){
            Image im1 = new Image("Imagenes/cellCompleto.png");
            Lighting lighting = new Lighting();
            img12.setEffect(lighting);img2.setImage(im1);img2.setFitHeight(320);img2.setFitWidth(400);
            img9.setDisable(true);img10.setDisable(true);img11.setDisable(true);
            img12.setDisable(true);img13.setDisable(true);img14.setDisable(true);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
            btnLimpiar2.setDisable(false);
    }
            else if(event.getSource().equals(img13)){
            Image im1 = new Image("Imagenes/buuCompleto.png");
            Lighting lighting = new Lighting();
            img13.setEffect(lighting);img2.setImage(im1);img2.setFitHeight(320);img2.setFitWidth(400);
            img9.setDisable(true);img10.setDisable(true);img11.setDisable(true);
            img12.setDisable(true);img13.setDisable(true);img14.setDisable(true);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
            btnLimpiar2.setDisable(false);
    }
            else if(event.getSource().equals(img14)){
            Image im1 = new Image("Imagenes/gohanCompleto.png");
            Lighting lighting = new Lighting();
            img14.setEffect(lighting);img2.setImage(im1);img2.setFitHeight(320);img2.setFitWidth(400);
            img9.setDisable(true);img10.setDisable(true);img11.setDisable(true);
            img12.setDisable(true);img13.setDisable(true);img14.setDisable(true);
            imgc1.setDisable(false);imgc2.setDisable(false);imgc3.setDisable(false);
            btnLimpiar2.setDisable(false);
           
    }
    }

    @FXML
    void presionar3(MouseEvent event) {
        if(event.getSource().equals(imgc1)){
            Image im1 = new Image("Imagenes/fondocombate4.jpg");
            Lighting lighting = new Lighting(); Blend b=new Blend();
            imgc2.setEffect(b);imgc3.setEffect(b);
            imgc1.setEffect(lighting);imgc4.setImage(im1);imgc4.setFitHeight(200);imgc4.setFitWidth(400);
            imgc1.setDisable(true);imgc2.setDisable(true);imgc3.setDisable(true);
            btnEmpezar.setDisable(false);
            seleccion=1;
    }
        else if(event.getSource().equals(imgc2)){
            Image im1 = new Image("Imagenes/fondocombate3.jpg");
            Lighting lighting = new Lighting();
            Blend b=new Blend();
            imgc1.setEffect(b);imgc3.setEffect(b);
            imgc2.setEffect(lighting);imgc4.setImage(im1);imgc4.setFitHeight(200);imgc4.setFitWidth(400);
            imgc1.setDisable(true);imgc2.setDisable(true);imgc3.setDisable(true);
            btnEmpezar.setDisable(false);
            seleccion=2;            
    }
        else if(event.getSource().equals(imgc3)){
            Image im1 = new Image("Imagenes/fondocombate2.jpg");
            Lighting lighting = new Lighting();
            Blend b=new Blend();
            imgc1.setEffect(b);imgc2.setEffect(b);
            imgc3.setEffect(lighting);imgc4.setImage(im1);imgc4.setFitHeight(200);imgc4.setFitWidth(400);
            imgc1.setDisable(true);imgc2.setDisable(true);imgc3.setDisable(true);
            btnEmpezar.setDisable(false);
            seleccion=3;
    }
    }

    @FXML
    void presionarImagen(MouseEvent event) {
        try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("FXMLDocument.fxml"));
                Scene scene = new Scene(fxmlLoader.load(), 650,381);
                Stage stage = new Stage();
                stage.setTitle("");
                stage.setScene(scene);
                stage.show();
                Stage myStage = (Stage) this.imgFlecha.getScene().getWindow();
                myStage.close();
                

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }

    @FXML
        void empezarCombate(ActionEvent event) throws InterruptedException {
            try {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(getClass().getResource("personajeSeleccionado.fxml"));
                    Scene scene = new Scene(fxmlLoader.load(), 851  , 560);
                    Stage stage = new Stage();stage.setTitle("COMBATE DRAGON BALL");
                    stage.setScene(scene);stage.show();
                    Stage myStage = (Stage) this.btnEmpezar.getScene().getWindow();
                    myStage.close();
                    
                    PersonajeSeleccionadoController seleccionadoController = (PersonajeSeleccionadoController)fxmlLoader.getController();
                    if(label1.getText().equals("GOKU")){
                        seleccionadoController.img1.setImage(new Image("Imagenes/gokuFondo.jpg")); 
                        seleccionadoController.txt1.setText("GOKU");
                  
                    }
                    
                    if(label1.getText().equals("TRUNKS")){
                        seleccionadoController.img1.setImage(new Image("Imagenes/trunksFondo.png"));
                        seleccionadoController.txt1.setText("TRUNKS");
         
                        
                    }
                    if(label1.getText().equals("VEGETA")){
                        seleccionadoController.img1.setImage(new Image("Imagenes/vegetaFondo.jpg"));
                        seleccionadoController.txt1.setText("VEGETA");

                    }
                    if(label1.getText().equals("CELL")){
                        seleccionadoController.img1.setImage(new Image("Imagenes/cellFondo.jpg"));
                        seleccionadoController.txt1.setText("CELL");
                       
                    }
                    if(label1.getText().equals("KID BUU")){
                        seleccionadoController.img1.setImage(new Image("Imagenes/buuFondo.jpg"));
                        seleccionadoController.txt1.setText("KID BUU");
                      
                    }
                    if(label1.getText().equals("GOHAN")){
                        seleccionadoController.img1.setImage(new Image("Imagenes/gohanFondo.jpg"));
                        seleccionadoController.txt1.setText("GOHAN");
                        
                    }
                    
                    if(label2.getText().equals("GOKU")){
                        seleccionadoController.img2.setImage(new Image("Imagenes/gokuFondo.jpg")); 
                        seleccionadoController.txt2.setText("GOKU");
                    }
                    
                    if(label2.getText().equals("TRUNKS")){
                        seleccionadoController.img2.setImage(new Image("Imagenes/trunksFondo.png"));
                        seleccionadoController.txt2.setText("TRUNKS");
                    }
                    if(label2.getText().equals("VEGETA")){
                        seleccionadoController.img2.setImage(new Image("Imagenes/vegetaFondo.jpg"));
                        seleccionadoController.txt2.setText("VEGETA");
                    }
                    if(label2.getText().equals("CELL")){
                        seleccionadoController.img2.setImage(new Image("Imagenes/cellFondo.jpg"));
                        seleccionadoController.txt2.setText("CELL");
                    }
                    if(label2.getText().equals("KID BUU")){
                        seleccionadoController.img2.setImage(new Image("Imagenes/buuFondo.jpg"));
                        seleccionadoController.txt2.setText("KID BUU");
                    }
                    if(label2.getText().equals("GOHAN")){
                        seleccionadoController.img2.setImage(new Image("Imagenes/gohanFondo.jpg"));
                        seleccionadoController.txt2.setText("GOHAN");
                    }
                    
                    if(seleccion==1){
                       seleccionadoController.txt3.setText("CAMPO1");
                    }
                    if(seleccion==2){
                        seleccionadoController.txt3.setText("CAMPO2");
                    }
                    if(seleccion==3){
                        seleccionadoController.txt3.setText("CAMPO3");
                    }
                    
                    
            } catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
        }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
            btnEmpezar.setDisable(true);btnLimpiar.setDisable(true);
            btnLimpiar2.setDisable(true);imgc1.setDisable(true);
            imgc2.setDisable(true);imgc3.setDisable(true);img9.setDisable(true);
            img10.setDisable(true);img11.setDisable(true);img12.setDisable(true);
            img13.setDisable(true);img14.setDisable(true);
    }

}