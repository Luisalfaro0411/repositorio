/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package videojuego;

/**
 *
 * @author 34698
 */
public class combateAtributos {
    private String player1;
    private String player2;
    private String ganadorCombate;

    public combateAtributos(String player1, String player2, String ganadorCombate) {
        this.player1 = player1;
        this.player2 = player2;
        this.ganadorCombate = ganadorCombate;
    }

    public String getPlayer1() {
        return player1;
    }

    public void setPlayer1(String player1) {
        this.player1 = player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public void setPlayer2(String player2) {
        this.player2 = player2;
    }

    public String getGanadorCombate() {
        return ganadorCombate;
    }

    public void setGanadorCombate(String ganadorCombate) {
        this.ganadorCombate = ganadorCombate;
    }
    
    
}
