/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videojuego;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.scene.input.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class FXMLDocumentController implements Initializable{

    @FXML
    private Label label;
    
    @FXML
    private Button btn1;

    @FXML
    void iniciarVideojuego(ActionEvent event) {
       
        try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("Personajes.fxml"));
                Scene scene = new Scene(fxmlLoader.load(), 966   , 609);
                Stage stage = new Stage();
                stage.setTitle("");
                stage.setScene(scene);
                stage.show();
                Stage myStage = (Stage) this.btn1.getScene().getWindow();
                myStage.close();
                

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        ObservableAux.inicializar();
    }

}
