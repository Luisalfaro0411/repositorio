/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videojuego;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class Ganador2Controller implements Initializable{

    @FXML
    private Button btnPrincipal;

    @FXML
    public ImageView img2;
    
    @FXML
    public Label txt1;

    @FXML
    public Label txt2;
    
    @FXML
    public Label txt3;

    @FXML
    void regresarVentana(ActionEvent event) {
        try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("cuadroResultado.fxml"));
                Scene scene = new Scene(fxmlLoader.load(), 711  , 412);
                Stage stage = new Stage();
                stage.setTitle("");
                stage.setScene(scene);
                stage.show();
                Stage myStage = (Stage) this.btnPrincipal.getScene().getWindow();
                myStage.close();
                CuadroResultadoController secondaryController=(CuadroResultadoController)fxmlLoader.getController();
                String player1= txt1.getText();
                String player2= txt2.getText();
                String ganador= txt3.getText();
                combateAtributos p=new combateAtributos(player1, player2, ganador + " - PLAYER2");
                secondaryController.combate.add(p);
                secondaryController.tvResultado.setItems(secondaryController.combate);
                ObservableAux.add(new combateAtributos(player1, player2,ganador + " - PLAYER2"));
                secondaryController.tvResultado.setItems(ObservableAux.getAux());
           
        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnPrincipal.setVisible(false);
        txt1.setVisible(false);
        txt2.setVisible(false);
        txt3.setVisible(false);
        delay(3000, () -> btnPrincipal.fire());
    }
    
    public static void delay(long millis, Runnable continuation) {
        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> continuation.run());
        new Thread(sleeper).start();
    }

}

