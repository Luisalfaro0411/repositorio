/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videojuego;

/**
 *
 * @author Mañana_pos5
 */
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ObservableAux {

    CuadroResultadoController combate;
    protected static ObservableList<combateAtributos> aux;

    public static void inicializar() {
        aux = FXCollections.observableArrayList();
    }

    public static void add(combateAtributos stats) {
        aux.add(stats);
    }
    
    protected static ObservableList getAux(){
        return aux;
    }
}

