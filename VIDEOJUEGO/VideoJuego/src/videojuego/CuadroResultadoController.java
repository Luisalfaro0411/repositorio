    /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package videojuego;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import static videojuego.Ganador2Controller.delay;

public class CuadroResultadoController implements Initializable{

    @FXML
    private Button btnAgregar;
    
    @FXML
    private ImageView img1;

    @FXML
    private ImageView img2;

    @FXML
    private ImageView imgFlecha;

    @FXML
    public TableView<combateAtributos> tvResultado;

    @FXML
    private Label txt1;

    @FXML
    private Label txt2;
    
     @FXML
    public ObservableList<combateAtributos> combate;
     
     @FXML
    private TableColumn colFinal;

    @FXML
    private TableColumn coljugador1;

    @FXML
    private TableColumn coljugador2;

    private int posicionPersonaEnTabla;
    
    
    
    @FXML
    void regresarVentana(MouseEvent event) {
        try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("Personajes.fxml"));
                Scene scene = new Scene(fxmlLoader.load(), 966   , 609);
                Stage stage = new Stage();
                stage.setTitle("");
                stage.setScene(scene);
                stage.show();
                Stage myStage = (Stage) this.imgFlecha.getScene().getWindow();
                myStage.close();
                tvResultado.setItems(ObservableAux.getAux());
                

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }

     public static void delay(long millis, Runnable continuation) {
        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> continuation.run());
        new Thread(sleeper).start();
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        combate = FXCollections.observableArrayList();
        this.coljugador1.setCellValueFactory(new PropertyValueFactory("player1"));
        this.coljugador2.setCellValueFactory(new PropertyValueFactory("player2"));
        this.colFinal.setCellValueFactory(new PropertyValueFactory("ganadorCombate"));
        final ObservableList<combateAtributos> tablaPersonaSel = tvResultado.getSelectionModel().getSelectedItems();
        tablaPersonaSel.addListener(selectorTablaPersonas);      
        img1.setImage(null);
        img2.setImage(null);
        tvResultado.setItems(ObservableAux.getAux());
    }
    
    private final ListChangeListener<combateAtributos> selectorTablaPersonas =
            new ListChangeListener<combateAtributos>() {
                @Override
                public void onChanged(ListChangeListener.Change<? extends combateAtributos> c) {
                    ponerPersonaSeleccionada();
                }
            };

    public combateAtributos getTablaPersonasSeleccionada() {
        if (tvResultado != null) {
            List<combateAtributos> tabla = tvResultado.getSelectionModel().getSelectedItems();
            if (tabla.size() == 1) {
                final combateAtributos competicionSeleccionada = tabla.get(0);
                return competicionSeleccionada;
            }
        }
        return null;
    }

    private void ponerPersonaSeleccionada() {
        final combateAtributos persona = getTablaPersonasSeleccionada();
        posicionPersonaEnTabla = combate.indexOf(persona);

        if (persona != null) {

            txt1.setText(persona.getPlayer1());
            txt2.setText(persona.getPlayer2());
            
            if(persona.getPlayer1().equals("GOKU")){
             img1.setImage(new Image("Imagenes/gokuCompleto.png"));
            }
            else if(persona.getPlayer1().equals("TRUNKS")){
             img1.setImage(new Image("Imagenes/trunksCompleto.png"));
            }
            else if(persona.getPlayer1().equals("VEGETA")){
             img1.setImage(new Image("Imagenes/vegetaCompleto.png"));
            }
            else if(persona.getPlayer1().equals("CELL")){
             img1.setImage(new Image("Imagenes/cellCompleto.png"));
            }
            else if(persona.getPlayer1().equals("KID BUU")){
             img1.setImage(new Image("Imagenes/buuCompleto.png"));
            }
            else if(persona.getPlayer1().equals("GOHAN")){
             img1.setImage(new Image("Imagenes/gohanCompleto.png"));
            }
            if(persona.getPlayer2().equals("GOKU")){
             img2.setImage(new Image("Imagenes/gokuCompleto.png"));
            }
            else if(persona.getPlayer2().equals("TRUNKS")){
             img2.setImage(new Image("Imagenes/trunksCompleto.png"));
            }
            else if(persona.getPlayer2().equals("VEGETA")){
             img2.setImage(new Image("Imagenes/vegetaCompleto.png"));
            }
            else if(persona.getPlayer2().equals("CELL")){
             img2.setImage(new Image("Imagenes/cellCompleto.png"));
            }
            else if(persona.getPlayer2().equals("KID BUU")){
             img2.setImage(new Image("Imagenes/buuCompleto.png"));
            }
            else if(persona.getPlayer2().equals("GOHAN")){
             img2.setImage(new Image("Imagenes/gohanCompleto.png"));
            }
            

        }
    }

}