
package videojuego;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

public class CombateController implements Initializable{

    private double vidaactual;
    private double vidaactual1;
    private double vidaactuala;
    private double vidaespecial;
    private double vidaespeciala;
    private double vidaespecialb;
    private double vidaespecial1;
    
    @FXML
    private Button btnfin;
    
    @FXML
    public ImageView fondo1;
    
    @FXML
    public ImageView fondot;

    @FXML
    public ImageView fondot2;
    
    @FXML
    private ImageView img1retro;
    
    @FXML
    public Label labelE;

    @FXML
    public Label labelE1;
    
    @FXML
    public ProgressBar barra1;

    @FXML
    public ProgressBar barra2;

    @FXML
    private ProgressBar barra3;

    @FXML
    private ProgressBar barra4;
    
    @FXML
    private Button btna1;

    @FXML
    private Button btna2;

    @FXML
    private Button btna3;

    @FXML
    private Button btna4;

    @FXML
    private Button btnae;

    @FXML
    private Button btnae1;
    
     @FXML
    private ImageView imgkame1;

    @FXML
    private ImageView imgkame2;

    
    @FXML
    void retrocederS(MouseEvent event) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("Personajes.fxml"));
                Scene scene = new Scene(fxmlLoader.load(), 966,609);
                Stage stage = new Stage();
                stage.setTitle("");
                stage.setScene(scene);
                stage.show();
                Stage myStage = (Stage) this.img1retro.getScene().getWindow();
                myStage.close();
                

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }
    
    
    
     @FXML
    void atacar1(ActionEvent event) {
        if(event.getSource().equals(btna1)){
            
        if(labelE.getText().equals("GOKU")){
       
        btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
             delay(3500, () -> btnae1.setDisable(false));
        }
        fondot.setImage(new Image("Imagenes/gokuataquederecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        TranslateTransition translate = new TranslateTransition();
        translate.setNode(fondot);translate.setDuration(Duration.millis(1300));translate.setFromX(0);
        translate.setByX(600);translate.setCycleCount(2);translate.setAutoReverse(true);translate.play();
        delay(1300, () -> fondot2.setImage(new Image("Imagenes/gokuatacadoizquierda.gif")));
        delay(1300, () -> fondot2.setScaleX(1.1));
        delay(1300, () -> fondot2.setScaleY(1.1));
        delay(3400, () -> fondot2.setImage(new Image("Imagenes/gokuizquierda.gif")));
        delay(3500, () -> fondot.setImage(new Image("Imagenes/gokuderecha.gif")));
        delay(3500, () -> btna3.setDisable(false));
        delay(3500, () -> btna4.setDisable(false));
        barravidaPersonaje();
        
        }
        
        else if(labelE.getText().equals("TRUNKS")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/trunksderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonaje();
        }
        else if(labelE.getText().equals("VEGETA")){
        btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/vegetaderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonaje();
        
        }
        else if(labelE.getText().equals("CELL")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/cellderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonaje();
        }
        else if(labelE.getText().equals("KID BUU")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/buuderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonaje();
        }
        else if(labelE.getText().equals("GOHAN")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/gohanderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonaje();
        }
        }
        
         if(barra2.getProgress()<=0.00){
            try{
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("Ganador1.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 601  , 371);
            Stage stage = new Stage();stage.setTitle("");
            stage.setScene(scene);stage.show();
            Stage myStage = (Stage) this.btna1.getScene().getWindow();
            myStage.close();
            Ganador1Controller secondaryController=(Ganador1Controller)fxmlLoader.getController();
            delay(3500, () -> secondaryController.im1.setImage(new Image("Imagenes/winner1.jpg")));
            if(labelE.getText().equals("GOKU")){
            secondaryController.txt1.setText("GOKU");
            secondaryController.txt3.setText("GOKU");
            }
        
        else if(labelE.getText().equals("TRUNKS")){
            secondaryController.txt1.setText("TRUNKS");
            secondaryController.txt3.setText("TRUNKS"); 
        }
        else if(labelE.getText().equals("VEGETA")){
            secondaryController.txt1.setText("VEGETA");
             secondaryController.txt3.setText("VEGETA");
        }
        else if(labelE.getText().equals("CELL")){
         secondaryController.txt1.setText("CELL");
         secondaryController.txt3.setText("CELL"); 
        }
        else if(labelE.getText().equals("KID BUU")){
        secondaryController.txt1.setText("KID BUU");
        secondaryController.txt3.setText("KID BUU");
        }
        else if(labelE.getText().equals("GOHAN")){
         secondaryController.txt1.setText("GOHAN");
         secondaryController.txt3.setText("GOHAN");
        }
         if(labelE1.getText().equals("GOKU")){
            secondaryController.txt2.setText("GOKU");        
        }
        
        else if(labelE1.getText().equals("TRUNKS")){
            secondaryController.txt2.setText("TRUNKS");   
        }
        else if(labelE1.getText().equals("VEGETA")){
            secondaryController.txt2.setText("VEGETA");
        }
        else if(labelE1.getText().equals("CELL")){
         secondaryController.txt2.setText("CELL");   
        }
        else if(labelE1.getText().equals("KID BUU")){
        secondaryController.txt2.setText("KID BUU");   
        }
        else if(labelE1.getText().equals("GOHAN")){
         secondaryController.txt2.setText("GOHAN");   
        }
            //delay(3500, () -> fondot2.setImage(new Image("Imagenes/gokumuertoizquierda.gif")));
            }catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
        }
         
           
    }
    
    @FXML
    void atacar3(ActionEvent event) {
        
        if(event.getSource().equals(btna3)){
        if(labelE1.getText().equals("GOKU")){
        btna3.setDisable(true);btna4.setDisable(true);
        
        if(barra3.getProgress()==1.0){
            delay(3500, () -> btnae.setDisable(false));
        }
        fondot2.setImage(new Image("Imagenes/gokuataque1izquierda.gif"));
        fondot2.setScaleX(1.1);
        fondot2.setScaleY(1.1);
        TranslateTransition translate = new TranslateTransition();
        translate.setNode(fondot2);
        translate.setDuration(Duration.millis(1300));
        translate.setFromX(0);
        translate.setByX(-600);  
        translate.setCycleCount(2);  
        translate.setAutoReverse(true); 
        translate.play();
        delay(1300, () -> fondot.setImage(new Image("Imagenes/gokuatacadoderecha.gif")));
        delay(1300, () -> fondot.setScaleX(1.1));
        delay(1300, () -> fondot.setScaleY(1.1));
        delay(3400, () -> fondot.setImage(new Image("Imagenes/gokuderecha.gif")));
        delay(3500, () -> fondot2.setImage(new Image("Imagenes/gokuizquierda.gif")));
        barravidaPersonaje1();
        delay(3500, () -> btna1.setDisable(false));
        delay(3500, () -> btna2.setDisable(false));
        
        }
        else if(labelE1.getText().equals("TRUNKS")){
          btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/trunksizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonaje1();
        }
        else if(labelE1.getText().equals("VEGETA")){
        btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/vegetaizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonaje1();
        
        }
        
        else if(labelE1.getText().equals("CELL")){
         btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/cellizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonaje1();
        }
        else if(labelE1.getText().equals("KID BUU")){
         btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/buuizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonaje1();
        }
        else if(labelE1.getText().equals("GOHAN")){
         btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/gohanizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonaje1();
        }
        
        }
         if(barra1.getProgress()<=0.00){
            try{
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("Ganador2.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 597   , 370);
            Stage stage = new Stage();stage.setTitle("");
            stage.setScene(scene);stage.show();
            Stage myStage = (Stage) this.btna3.getScene().getWindow();
            myStage.close();
            Ganador2Controller secondaryController=(Ganador2Controller)fxmlLoader.getController();
            delay(3500, () -> secondaryController.img2.setImage(new Image("Imagenes/winner2.jpg")));
            if(labelE.getText().equals("GOKU")){
            secondaryController.txt1.setText("GOKU");
             
            }
        
        else if(labelE.getText().equals("TRUNKS")){
            secondaryController.txt1.setText("TRUNKS");
            
        }
        else if(labelE.getText().equals("VEGETA")){
            secondaryController.txt1.setText("VEGETA");
            
        }
        else if(labelE.getText().equals("CELL")){
         secondaryController.txt1.setText("CELL");
         
        }
        else if(labelE.getText().equals("KID BUU")){
        secondaryController.txt1.setText("KID BUU");   
        }
        else if(labelE.getText().equals("GOHAN")){
         secondaryController.txt1.setText("GOHAN");   
        }
         if(labelE1.getText().equals("GOKU")){
            secondaryController.txt2.setText("GOKU");
            secondaryController.txt3.setText("GOKU");
        }
        
        else if(labelE1.getText().equals("TRUNKS")){
            secondaryController.txt2.setText("TRUNKS");
            secondaryController.txt3.setText("TRUNKS");
        }
        else if(labelE1.getText().equals("VEGETA")){
            secondaryController.txt2.setText("VEGETA");
            secondaryController.txt3.setText("VEGETA");
        }
        else if(labelE1.getText().equals("CELL")){
         secondaryController.txt2.setText("CELL");
         secondaryController.txt3.setText("CELL");
        }
        else if(labelE1.getText().equals("KID BUU")){
        secondaryController.txt2.setText("KID BUU");
        secondaryController.txt3.setText("KID BUU");
        }
        else if(labelE1.getText().equals("GOHAN")){
         secondaryController.txt2.setText("GOHAN");
         secondaryController.txt3.setText("GOHAN");
        }
            //delay(3500, () -> fondot2.setImage(new Image("Imagenes/gokumuertoizquierda.gif")));
            }catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
        }
       
        
        }
       
    @FXML
    void atacar2(ActionEvent event) { 
        
       if(event.getSource().equals(btna2)){ 
           
       if(labelE.getText().equals("GOKU")){
        btna1.setDisable(true);btna2.setDisable(true);
        if(barra4.getProgress()==1.0){
             delay(3500, () -> btnae1.setDisable(false));
        }
        fondot.setImage(new Image("Imagenes/gokuataquederecha22.gif"));
        fondot.setScaleX(1.1);
        fondot.setScaleY(1.1);
        TranslateTransition translate = new TranslateTransition();
        translate.setNode(fondot);
        translate.setDuration(Duration.millis(1300));
        translate.setFromX(0);
        translate.setByX(600);  
        translate.setCycleCount(2);  
        translate.setAutoReverse(true); 
        translate.play();
        delay(1300, () -> fondot2.setImage(new Image("Imagenes/gokuatacadoizquierda.gif")));
        delay(1300, () -> fondot2.setScaleX(1.1));
        delay(1300, () -> fondot2.setScaleY(1.1));
        delay(3400, () -> fondot2.setImage(new Image("Imagenes/gokuizquierda.gif")));
        delay(3500, () -> fondot.setImage(new Image("Imagenes/gokuderecha.gif")));
        barravidaPersonaje();
        delay(3500, () -> btna3.setDisable(false));
        delay(3500, () -> btna4.setDisable(false));
        
        
        
        }
       
        else if(labelE.getText().equals("TRUNKS")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/trunksderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonaje();
        }
        else if(labelE.getText().equals("VEGETA")){
        btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/vegetaderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonaje();
        
        }
        else if(labelE.getText().equals("CELL")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/cellderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonaje();
        }
        else if(labelE.getText().equals("KID BUU")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/buuderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonaje();
        }
        else if(labelE.getText().equals("GOHAN")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/gohanderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonaje();
        }
       }
       
        if(barra2.getProgress()<=0.00){
            try{
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("Ganador1.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 601  , 371);
            Stage stage = new Stage();stage.setTitle("");
            stage.setScene(scene);stage.show();
            Stage myStage = (Stage) this.btna2.getScene().getWindow();
            myStage.close();
            Ganador1Controller secondaryController=(Ganador1Controller)fxmlLoader.getController();
            delay(3500, () -> secondaryController.im1.setImage(new Image("Imagenes/winner1.jpg")));
            //delay(3500, () -> fondot2.setImage(new Image("Imagenes/gokumuertoizquierda.gif")));
            if(labelE.getText().equals("GOKU")){
            secondaryController.txt1.setText("GOKU");
            secondaryController.txt3.setText("GOKU");
            }
        
        else if(labelE.getText().equals("TRUNKS")){
            secondaryController.txt1.setText("TRUNKS");
            secondaryController.txt3.setText("TRUNKS"); 
        }
        else if(labelE.getText().equals("VEGETA")){
            secondaryController.txt1.setText("VEGETA");
             secondaryController.txt3.setText("VEGETA");
        }
        else if(labelE.getText().equals("CELL")){
         secondaryController.txt1.setText("CELL");
         secondaryController.txt3.setText("CELL"); 
        }
        else if(labelE.getText().equals("KID BUU")){
        secondaryController.txt1.setText("KID BUU");
        secondaryController.txt3.setText("KID BUU");
        }
        else if(labelE.getText().equals("GOHAN")){
         secondaryController.txt1.setText("GOHAN");
         secondaryController.txt3.setText("GOHAN");
        }
         if(labelE1.getText().equals("GOKU")){
            secondaryController.txt2.setText("GOKU");        
        }
        
        else if(labelE1.getText().equals("TRUNKS")){
            secondaryController.txt2.setText("TRUNKS");   
        }
        else if(labelE1.getText().equals("VEGETA")){
            secondaryController.txt2.setText("VEGETA");
        }
        else if(labelE1.getText().equals("CELL")){
         secondaryController.txt2.setText("CELL");   
        }
        else if(labelE1.getText().equals("KID BUU")){
        secondaryController.txt2.setText("KID BUU");   
        }
        else if(labelE1.getText().equals("GOHAN")){
         secondaryController.txt2.setText("GOHAN");   
        }
            }catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
            
        }
    }

    @FXML
    void atacar4(ActionEvent event) {
        
        if(event.getSource().equals(btna4)){
        if(labelE1.getText().equals("GOKU")){
        btna3.setDisable(true);btna4.setDisable(true);
        if(barra3.getProgress()==1.0){
            delay(3500, () -> btnae.setDisable(false));
        }
        fondot2.setImage(new Image("Imagenes/gokuataqueizquierda2.gif"));
        fondot2.setScaleX(1.1);
        fondot2.setScaleY(1.1);
        TranslateTransition translate = new TranslateTransition();
        translate.setNode(fondot2);
        translate.setDuration(Duration.millis(1300));
        translate.setFromX(0);
        translate.setByX(-600);  
        translate.setCycleCount(2);  
        translate.setAutoReverse(true); 
        translate.play();
        delay(1300, () -> fondot.setImage(new Image("Imagenes/gokuatacadoderecha.gif")));
        delay(1300, () -> fondot.setScaleX(1.1));
        delay(1300, () -> fondot.setScaleY(1.1));
        delay(3400, () -> fondot.setImage(new Image("Imagenes/gokuderecha.gif")));
        delay(3500, () -> fondot2.setImage(new Image("Imagenes/gokuizquierda.gif")));
        barravidaPersonaje1();
        delay(3500, () -> btna1.setDisable(false));
        delay(3500, () -> btna2.setDisable(false));
        
        }
        else if(labelE1.getText().equals("TRUNKS")){
          btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/trunksizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonaje1();
        }
        else if(labelE1.getText().equals("VEGETA")){
        btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/vegetaizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonaje1();
        
        }
        else if(labelE1.getText().equals("CELL")){
         btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/cellizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonaje1();
        }
        else if(labelE1.getText().equals("KID BUU")){
         btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/buuizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonaje1();
        }
        else if(labelE1.getText().equals("GOHAN")){
         btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/gohanizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonaje1();
        }
        }
        
        if(barra1.getProgress()<= 0.00){
            
            try{
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("Ganador2.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 597   , 370);
            Stage stage = new Stage();stage.setTitle("");
            stage.setScene(scene);stage.show();
            Stage myStage = (Stage) this.btna4.getScene().getWindow();
            myStage.close();
            Ganador2Controller secondaryController=(Ganador2Controller)fxmlLoader.getController();
            delay(3500, () -> secondaryController.img2.setImage(new Image("Imagenes/winner2.jpg")));
            if(labelE.getText().equals("GOKU")){
            secondaryController.txt1.setText("GOKU");
             
            }
        
        else if(labelE.getText().equals("TRUNKS")){
            secondaryController.txt1.setText("TRUNKS");
            
        }
        else if(labelE.getText().equals("VEGETA")){
            secondaryController.txt1.setText("VEGETA");
            
        }
        else if(labelE.getText().equals("CELL")){
         secondaryController.txt1.setText("CELL");
         
        }
        else if(labelE.getText().equals("KID BUU")){
        secondaryController.txt1.setText("KID BUU");   
        }
        else if(labelE.getText().equals("GOHAN")){
         secondaryController.txt1.setText("GOHAN");   
        }
         if(labelE1.getText().equals("GOKU")){
            secondaryController.txt2.setText("GOKU");
            secondaryController.txt3.setText("GOKU");
        }
        
        else if(labelE1.getText().equals("TRUNKS")){
            secondaryController.txt2.setText("TRUNKS");
            secondaryController.txt3.setText("TRUNKS");
        }
        else if(labelE1.getText().equals("VEGETA")){
            secondaryController.txt2.setText("VEGETA");
            secondaryController.txt3.setText("VEGETA");
        }
        else if(labelE1.getText().equals("CELL")){
         secondaryController.txt2.setText("CELL");
         secondaryController.txt3.setText("CELL");
        }
        else if(labelE1.getText().equals("KID BUU")){
        secondaryController.txt2.setText("KID BUU");
        secondaryController.txt3.setText("KID BUU");
        }
        else if(labelE1.getText().equals("GOHAN")){
         secondaryController.txt2.setText("GOHAN");
         secondaryController.txt3.setText("GOHAN");
        }
            }catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
        }        
    }

    @FXML
    void atacarEspecial(ActionEvent event) {
        barra3.setProgress(0.0);
        btna1.setDisable(true);btna2.setDisable(true);btnae.setDisable(true);
        
        if(barra4.getProgress()==1.0){
            delay(9100, () -> btnae1.setDisable(false));
        }
        else{
            btnae1.setDisable(true);
        }
        if(labelE.getText().equals("GOKU")){
        fondot.setImage(new Image("Imagenes/gokuataquefinalderecha.gif"));
        fondot.setScaleX(1.1);
        fondot.setScaleY(1.1);
        delay(5500, () -> imgkame1.setImage(new Image("Imagenes/kamederecha.gif")));
        delay(5500, () -> imgkame1.setVisible(true));
        TranslateTransition translate = new TranslateTransition();
        translate.setNode(imgkame1); 
        delay(5500, () -> translate.setDuration(Duration.millis(1300)));
        delay(5500, () -> translate.setFromX(0));
        delay(5500, () -> translate.setToX(400));
        delay(5500, () -> translate.play());
        delay(7000, () -> imgkame1.setVisible(false));
        delay(7000, () -> fondot2.setImage(new Image("Imagenes/gokuatacadoizquierda.gif")));
        delay(7000, () -> fondot2.setScaleX(1.1));
        delay(7000, () -> fondot2.setScaleY(1.1));
        delay(9100, () -> fondot2.setImage(new Image("Imagenes/gokuizquierda.gif")));
        delay(6000, () -> fondot.setImage(new Image("Imagenes/gokuderecha.gif")));
        barravidaPersonajeespecial();
        delay(9100, () -> btna3.setDisable(false));
        delay(9100, () -> btna4.setDisable(false));
        }
        else if(labelE.getText().equals("TRUNKS")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/trunksderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonajeespecial();
        }
        else if(labelE.getText().equals("VEGETA")){
        btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/vegetaderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
       barravidaPersonajeespecial();
        
        }
        else if(labelE.getText().equals("CELL")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/cellderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonajeespecial();
        }
        else if(labelE.getText().equals("KID BUU")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/buuderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonajeespecial();
        }
        else if(labelE.getText().equals("GOHAN")){
         btna1.setDisable(true);btna2.setDisable(true);
         if(barra4.getProgress()==1.0){
            btnae1.setDisable(false);
        }
        fondot.setImage(new Image("Imagenes/gohanderecha.gif"));
        fondot.setScaleX(1.1);fondot.setScaleY(1.1);
        btna3.setDisable(false);
        btna4.setDisable(false);
        barravidaPersonajeespecial();
        }
       if(barra2.getProgress()<=0.00){
            try{
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("Ganador1.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 609   , 385);
            Stage stage = new Stage();stage.setTitle("");
            stage.setScene(scene);stage.show();
            Stage myStage = (Stage) this.btnae.getScene().getWindow();
            myStage.close();
            Ganador1Controller secondaryController=(Ganador1Controller)fxmlLoader.getController();
            delay(9100, () -> secondaryController.im1.setImage(new Image("Imagenes/winner1.jpg")));
            //delay(3500, () -> fondot2.setImage(new Image("Imagenes/gokumuertoizquierda.gif")));
             if(labelE.getText().equals("GOKU")){
            secondaryController.txt1.setText("GOKU");
            secondaryController.txt3.setText("GOKU");
            }
        
        else if(labelE.getText().equals("TRUNKS")){
            secondaryController.txt1.setText("TRUNKS");
            secondaryController.txt3.setText("TRUNKS"); 
        }
        else if(labelE.getText().equals("VEGETA")){
            secondaryController.txt1.setText("VEGETA");
             secondaryController.txt3.setText("VEGETA");
        }
        else if(labelE.getText().equals("CELL")){
         secondaryController.txt1.setText("CELL");
         secondaryController.txt3.setText("CELL"); 
        }
        else if(labelE.getText().equals("KID BUU")){
        secondaryController.txt1.setText("KID BUU");
        secondaryController.txt3.setText("KID BUU");
        }
        else if(labelE.getText().equals("GOHAN")){
         secondaryController.txt1.setText("GOHAN");
         secondaryController.txt3.setText("GOHAN");
        }
         if(labelE1.getText().equals("GOKU")){
            secondaryController.txt2.setText("GOKU");        
        }
        
        else if(labelE1.getText().equals("TRUNKS")){
            secondaryController.txt2.setText("TRUNKS");   
        }
        else if(labelE1.getText().equals("VEGETA")){
            secondaryController.txt2.setText("VEGETA");
        }
        else if(labelE1.getText().equals("CELL")){
         secondaryController.txt2.setText("CELL");   
        }
        else if(labelE1.getText().equals("KID BUU")){
        secondaryController.txt2.setText("KID BUU");   
        }
        else if(labelE1.getText().equals("GOHAN")){
         secondaryController.txt2.setText("GOHAN");   
        }
            }catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
            
        }
        
    }

    @FXML
    void atacarEspecial1(ActionEvent event) {
        
        barra4.setProgress(0.0);
        btna3.setDisable(true);btna4.setDisable(true);btnae1.setDisable(true);
        if(barra4.getProgress()==1.0){
            delay(9100, () -> btnae.setDisable(false));
        }
        else{
            btnae.setDisable(true);
        }
        if(labelE1.getText().equals("GOKU")){
        fondot2.setImage(new Image("Imagenes/gokuataquefinalizquierda.gif"));
        fondot2.setScaleX(1.1);
        fondot2.setScaleY(1.1);
        delay(5500, () -> imgkame2.setImage(new Image("Imagenes/kameizquierda.gif")));
        delay(5500, () -> imgkame2.setVisible(true));
        TranslateTransition translate = new TranslateTransition();
        translate.setNode(imgkame2); 
        delay(5500, () -> translate.setDuration(Duration.millis(1300)));
        delay(5500, () -> translate.setFromX(0));
        delay(5500, () -> translate.setToX(-400));
        delay(5500, () -> translate.play());
        delay(7000, () -> imgkame2.setVisible(false));
        delay(7000, () -> fondot.setImage(new Image("Imagenes/gokuatacadoderecha.gif")));
        delay(7000, () -> fondot.setScaleX(1.1));
        delay(7000, () -> fondot.setScaleY(1.1));
        delay(9100, () -> fondot.setImage(new Image("Imagenes/gokuderecha.gif")));
        delay(6000, () -> fondot2.setImage(new Image("Imagenes/gokuizquierda.gif")));
        barravidaPersonajeespecial1();
        delay(9100, () -> btna1.setDisable(false));
        delay(9100, () -> btna2.setDisable(false));
        }
        
        else if(labelE1.getText().equals("TRUNKS")){
          btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/trunksizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonajeespecial1();
        }
        else if(labelE1.getText().equals("VEGETA")){
        btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/vegetaizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonaje1();
        
        }
        else if(labelE1.getText().equals("CELL")){
         btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/cellizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonajeespecial1();
        }
        else if(labelE1.getText().equals("KID BUU")){
         btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/buuizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonajeespecial1();
        }
        else if(labelE1.getText().equals("GOHAN")){
         btna3.setDisable(true);btna4.setDisable(true);
         if(barra3.getProgress()==1.0){
            btnae.setDisable(false);
        }
        fondot2.setImage(new Image("Imagenes/gohanizquierda.gif"));
        fondot2.setScaleX(1.1);fondot2.setScaleY(1.1);
        btna1.setDisable(false);
        btna2.setDisable(false);
        barravidaPersonajeespecial1();
        }
        if(barra1.getProgress()<= 0.00){
            
            try{
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("Ganador2.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600  , 400);
            Stage stage = new Stage();stage.setTitle("");
            stage.setScene(scene);stage.show();
            Stage myStage = (Stage) this.btnae1.getScene().getWindow();
            myStage.close();
            Ganador2Controller secondaryController=(Ganador2Controller)fxmlLoader.getController();
            delay(9100, () -> secondaryController.img2.setImage(new Image("Imagenes/winner2.jpg")));
            if(labelE.getText().equals("GOKU")){
            secondaryController.txt1.setText("GOKU");
             
            }
        
        else if(labelE.getText().equals("TRUNKS")){
            secondaryController.txt1.setText("TRUNKS");
            
        }
        else if(labelE.getText().equals("VEGETA")){
            secondaryController.txt1.setText("VEGETA");
            
        }
        else if(labelE.getText().equals("CELL")){
         secondaryController.txt1.setText("CELL");
         
        }
        else if(labelE.getText().equals("KID BUU")){
        secondaryController.txt1.setText("KID BUU");   
        }
        else if(labelE.getText().equals("GOHAN")){
         secondaryController.txt1.setText("GOHAN");   
        }
         if(labelE1.getText().equals("GOKU")){
            secondaryController.txt2.setText("GOKU");
            secondaryController.txt3.setText("GOKU");
        }
        
        else if(labelE1.getText().equals("TRUNKS")){
            secondaryController.txt2.setText("TRUNKS");
            secondaryController.txt3.setText("TRUNKS");
        }
        else if(labelE1.getText().equals("VEGETA")){
            secondaryController.txt2.setText("VEGETA");
            secondaryController.txt3.setText("VEGETA");
        }
        else if(labelE1.getText().equals("CELL")){
         secondaryController.txt2.setText("CELL");
         secondaryController.txt3.setText("CELL");
        }
        else if(labelE1.getText().equals("KID BUU")){
        secondaryController.txt2.setText("KID BUU");
        secondaryController.txt3.setText("KID BUU");
        }
        else if(labelE1.getText().equals("GOHAN")){
         secondaryController.txt2.setText("GOHAN");
         secondaryController.txt3.setText("GOHAN");
        }
           
            }catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
        }      
        
    }
    
    public void barravidaPersonaje(){
        
        vidaespecial= vidaespecial + 0.25;
        double danio = (double) (Math.random()*7+1);
        vidaactual = vidaactual - danio;
        barra2.setProgress(vidaactual/50);
        barra3.setProgress(vidaespecial);
        if(barra3.getProgress()>1){
            vidaespeciala=vidaespeciala+0.25;
            barra3.setProgress(vidaespeciala);
        }
        
    }
    
    public void barravidaPersonaje1(){
        vidaespecial1= vidaespecial1 + 0.25;
        double danio = (double) (Math.random()*7+1);
        vidaactual1 = vidaactual1 - danio;
        barra1.setProgress(vidaactual1/50);
        barra4.setProgress(vidaespecial1);
        if(barra4.getProgress()>1){
            vidaespecialb=vidaespecialb+0.25;
            barra4.setProgress(vidaespecialb);
        }
    }
    
     public void barravidaPersonajeespecial1(){
        double danio = (double) (Math.random()*15+8);
        vidaactual1 = vidaactual1 - danio;
        barra1.setProgress(vidaactual1/50);
        barra4.setProgress(0.0);      
    }
     public void barravidaPersonajeespecial(){
        double danio = (double) (Math.random()*15+8);
        vidaactual = vidaactual - danio;
        barra2.setProgress(vidaactual/50);
        barra3.setProgress(0.0);       
    }
    
     public static void delay(long millis, Runnable continuation) {
        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> continuation.run());
        new Thread(sleeper).start();
    }

     public void barravida(){
        barra1.setProgress(1.0);
        barra2.setProgress(1.0);
        barra3.setProgress(0.0);
        barra4.setProgress(0.0);
        barra3.setStyle("-fx-accent: red");
        barra4.setStyle("-fx-accent: red");
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
                vidaactual=50;
                vidaactual1=50;
                barravida();
                btnae.setDisable(true);
                btnae1.setDisable(true);
                btna3.setDisable(true);
                btna4.setDisable(true);
                imgkame2.setVisible(false);
                imgkame1.setVisible(false);
                btnfin.setVisible(false);
                
    }
    
}
