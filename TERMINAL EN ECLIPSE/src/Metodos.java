import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Array;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.Writer;

public class Metodos {

	public static String ruta = System.getProperty("user.dir");
	public static File carpeta = new File(ruta);
	public static String rutaActual;
	public static String comando;
	public static String[] array;
	public static Scanner a;
	
	public static void menu() {
		System.out.println("LISTA DE COMANDOS");
		System.out.println("- help");
		System.out.println("- cd");
		System.out.println("- mkdir");
		System.out.println("- info");
		System.out.println("- cat");
		System.out.println("- top");
		System.out.println("- mkfile");
		System.out.println("- write");
		System.out.println("- dir");
		System.out.println("- readpoint");
		System.out.println("- delete");
		System.out.println("- close");
		metodos();	
	}
	
	
	public static void metodos() {
		Scanner t=new Scanner(System.in);
		String opcion;
			while(true) {
			System.out.println("INGRESAR EL COMANDO QUE DESEA UTILIZAR");
			comando=t.nextLine();
			array=comando.split(" ");
			
			switch(array[0].toLowerCase()) {
			
			case "help":
				menuJava();
				break;
				
			case "cd":
				cd(array);
				break;
				
			case "dir":
				dir();
				break;
				
			case "close":
				cerrar();
				break;
			
			case "cat":
				 cat(array);
				break;
			
			case "top":
				 top(array);
				break;
				
			case  "delete":
				eliminar();
				break;
				
			case "mkdir":
				mkdir();
				break;
			
			case "mkfile":
				mkfile();
				break;
				
			case "write":
				write(array);
				break;
				
			case "readpoint":
				readpoint(array);
				break;
				
			case "info":
				info(array);
				break;
				
			default:
				System.out.println("INGRESAR EL COMANDO CORRECTO");
				break;
			}
		}
		
	
	}
	
	public static void cd(String[] array) {
		
		if(array.length==1) {
			System.out.println(ruta);
			}
		
		else if(array[1].equals("..")) {					
			File ruta1 = new File(ruta);
			
			if (ruta1.getParent() != null) {
				rutaActual=ruta1.getParent();
				carpeta = new File(rutaActual);
				ruta=rutaActual;
				System.out.println(carpeta);
				
			}	
			
			else {
				System.out.println(ruta);
				
			}		
		}
		
		else if(array.length==2){
				File archivo = new File(ruta,array[1]);
					if(archivo.exists() && archivo.isDirectory()) {
						rutaActual=archivo.getAbsolutePath();
						carpeta = new File(rutaActual);
						ruta=rutaActual;
						System.out.println(carpeta);
				}
					else {
						System.out.println("CARPETA NO CREADA");

					}
		}	
		
	else {
		System.out.println("NO EJECUTABLE");
}
	}
				
		
	public static void mkfile() {
		File directorio12=new File (carpeta,array[1]);
		
		if (!directorio12.exists()) {
            try {
				directorio12.createNewFile();
				System.out.println("ARCHIVO CREADO");
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		else {
			System.out.println("ARCHIVO YA EXISTE O NO TIENE LA EXTENSION CORRECTA");
		}
		System.out.println("---------------------------------");

	}

	public static void mkdir() {
		File directorio1=new File (carpeta,array[1]);	
		if(!directorio1.mkdir()) {
			System.out.println("Error al crear el archivo");	
		}else {
			System.out.println("Directorio Creado");
		}
		System.out.println("---------------------------------");

	}
	
	public static void readpoint(String[]array) {
		if(array.length == 1) {
			System.out.println("INGRESAR EL COMANDO COMPLETO");
        }
        else {
        	try {
				RandomAccessFile randomAccessFile = new RandomAccessFile(carpeta + "\\" + array[1], "r");
				randomAccessFile.seek(Integer.parseInt(array[2]));
				String line = randomAccessFile.readLine();
				while (line != null) {
					System.out.println(line);
					line = randomAccessFile.readLine();
				}
				randomAccessFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		System.out.println("---------------------------------");

	}
	
	public static void dir() {
		
		for(String i: carpeta.list()) {
			System.out.println(i);
		}
		System.out.println("---------------------------------");
	}
	
	public static void cerrar() {
		System.out.println("PROGRAMA FINALIZADO");
		System.out.println("I--------------------------------");
		System.exit(0);
	}
	
	public static void menuJava() {
		System.out.println("- help -> lista todos comandos\".");
		System.out.println("-  cd -> muestra la ruta del directorio actual");
		System.out.println("  		- cd .. à Se mueve al directorio padre.");
		System.out.println("  		- cd + nombre à lista archivos de ese directorio.");
		System.out.println("- mkdir -> crea un directorio de la ruta actual");
		System.out.println("- info <nombre> -> Muestra la información del elemento Indicando:\"\r\n"
				+ "	+ \"\\n Nº of elements\" + \"\\n FreeSpace\" + \"\\n TotalSpace\" + \"\\n UsableSpace\"");
		System.out.println("- cat <nombreFichero> -> Muestra el contenido de un fichero.");
		System.out.println("- top <numeroLineas><NombreFichero> -> Muestra las líneas especificadas de un fichero.");
		System.out.println("- mkfile <nombreFichero> <texto> ->  Crea un fichero con ese nombre y el contenido de texto.");
		System.out.println("- write <nombreFichero> <texto>-> Añade 'texto' al final del fichero especificado.");
		System.out.println("- dir -> Lista los archivos o directorios de la ruta actual.");
		System.out.println("- readpoint <nombreFichero1> <posición> à Lee un archivo desde una determinada posición del puntero.");
		System.out.println("- delete <nombre>-> Borra el fichero, si es un directorio borra todo su contenido y a si mismo.");
		System.out.println("- close-> Cierra el programa.");
		System.out.println("---------------------------------------------");
	}
	
	public static void eliminar() {
		File fichero = new File(carpeta,array[1]);
		
		if(fichero.delete()) {
			System.out.println("El archivo ha sido eliminado satisfactoriamente");
		}
		else {
			System.out.println("El archivo no se ha eliminado");

		}
	}
	
	public static void cat(String[] array) {
		if(array.length == 2) {
			File file = new File(carpeta, array[1]);
        	try {
    			a = new Scanner(file);
    		} catch (FileNotFoundException e) {
    			e.printStackTrace();
    		}
        	while(a.hasNextLine()){
                System.out.println(a.nextLine());
            }
        }
	
        else {
        	System.out.println("INGRESAR EL COMANDO COMPLETO");
			System.out.println("");
		}
		}
	
	public static void top(String[] array) {
		
		if(array.length < 3) {
			System.out.println("INGRESAR EL COMANDO COMPLETO");
			System.out.println("");
	
        }
		
        else {
        	File file = new File(carpeta, array[1]);
        	try {
    			a = new Scanner(file);
    			
    		} catch (FileNotFoundException e) {
    			e.printStackTrace();
    		}
        	for (int i = 0; i < Integer.parseInt(array[2]); i++) {
        		System.out.println(a.nextLine());
        		
        	}
        }
	}
	
	public static void write(String[] array) {
		if(array.length == 1) {
			System.out.println("INGRESAR EL COMANDO COMPLETO");
        }
        else {
        	try {
				Writer output = new BufferedWriter(new FileWriter(ruta + "\\" + array[1], true));
				String texto = "";
				for (int i = 2; i < array.length; i++)
					texto += " " + array[i] ;
				output.append(texto);
				System.out.println("TEXTO AÑADIDO");
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
	}
	
	public static void info(String[] array) {
		if(array.length == 1) {
			System.out.println("INGRESAR EL COMANDO COMPLETO");
        }
        else {
        	File file = new File(carpeta, array[1]);
        	
			if (file.exists() && file.isDirectory()) {
				
				System.out.println("Nombre del archivo: " + file.getName());
				System.out.println("Ruta del archivo : " + file.getAbsolutePath());
				System.out.println("Espacio libre : " + file.getFreeSpace() + " B");
				System.out.println("Espacio Total : "+ file.getTotalSpace() + " B");
				
				File f = new File(carpeta, array[1]);
				int totalFiles=0;
				
				for (int i = 0; i < f.list().length; i++) 
					totalFiles++;
				System.out.println("Numero de Archivos: "+totalFiles);
			}
			else {
				System.out.println("El sistema no puede encontrar la ruta especificada.");
			}
        }
	}
	
}
