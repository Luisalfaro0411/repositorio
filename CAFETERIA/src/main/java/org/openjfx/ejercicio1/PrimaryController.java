package org.openjfx.Ejercicio1;


import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.crypto.AEADBadTagException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Cafe;

public class PrimaryController implements Initializable {
    
    

    @FXML
    private Button btnModificar;
    
    @FXML
    private Button btnBorrar;
    
    @FXML
    private Button btnPedir;

    @FXML
    private Button btnRecargar;

    @FXML
    private ComboBox<String> cbTipo;
    
    @FXML
    private TableColumn colCantidad;

    @FXML
    private TableColumn colNombre;

    @FXML
    private TableColumn colPrecio;

    @FXML
    private TableColumn colPrecioTotal;

    @FXML
    private TableColumn colTipo;

    @FXML
    private ImageView imgCafe;

    @FXML
    private RadioButton rb1;

    @FXML
    private RadioButton rb2;

    

    @FXML
    private RadioButton rb3;

    @FXML
    private TextField tfCantidad;

    @FXML
    private TextField tfPrecio;

    @FXML
    private TextField tfSaldo;
    
    @FXML
    private TextField txtTipo;
    
    @FXML
    private TextField txtsaldoActual;

    @FXML
    private TableView<Cafe> tvCafe;
    
    @FXML
    private ObservableList<Cafe> cafe;
    @FXML
    private ObservableList<Cafe> pedidos1 = FXCollections.observableArrayList();
    
    @FXML
    private Button btnfiltrar;
    
    @FXML
    private TextField txtBuscar;
    
    @FXML
    void buscarDato(ActionEvent event) {
            if (txtBuscar.getText().equalsIgnoreCase("")) {
                tvCafe.setItems((ObservableList)cafe);
    }
    else {
        pedidos1.clear();
        for (int i = 0; i < cafe.size() ; i++) {
            if (cafe.get(i).getNombre().equalsIgnoreCase(txtBuscar.getText())) 
               pedidos1.add(cafe.get(i));
        } 
        tvCafe.setItems((ObservableList)pedidos1);
    }
    }
    
    @FXML
    void modificarFila(ActionEvent event){
        
         Cafe p = this.tvCafe.getSelectionModel().getSelectedItem();

        if (p == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Debes seleccionar una persona");
            alert.showAndWait();
        } else {

            try {
                 double cantidad = Double.parseDouble(tfCantidad.getText());
                 Cafe c=new Cafe(nombreBotones(),cbTipo.getValue(),precioCafe(),cantidad,precioT());
                 
                
                    p.setNombre(c.getNombre());
                    p.setTipo(c.getTipo());
                    p.setPrecio(c.getPrecio());
                    p.setCantidad(c.getCantidad());
                    p.setPrecioT(c.getPrecioT());
                    if(precioT()>p.getPrecioT()){
                        txtsaldoActual.setText(Double.parseDouble(txtsaldoActual.getText()) - (c.getPrecioT() - p.getPrecioT()) + "");
                    }
                    else{
                        txtsaldoActual.setText(Double.parseDouble(txtsaldoActual.getText()) + (c.getPrecioT() - p.getPrecioT()) + "");

                    }
                    this.tvCafe.refresh();
                    

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setTitle("PEDIDO");
                    alert.setContentText("PEDIDO MODIFICADO");
                    alert.showAndWait();

                
            } catch (NumberFormatException e) {

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Error");
                alert.setContentText("Formato incorrecto");
                alert.showAndWait();
            }

        }        
         
        
    }
    
    @FXML
    void eliminarFila(ActionEvent event) {
        
        Cafe c=(Cafe) tvCafe.getSelectionModel().getSelectedItem();
         if(c == null){
             Alert alert1 = new Alert(Alert.AlertType.ERROR);
                    alert1.setHeaderText(null);
                    alert1.setTitle("ELIMINAR");
                    alert1.setContentText("DEBE DE SELECCIONAR UNA FILA O AGREGAR DATOS A LA TABLA");
         }
         else{
             Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setHeaderText(null);
                    alert.setTitle("ELIMINAR");
                    alert.setContentText("DESEA ELIMINAR DICHA COMPRA");
                    
                    
        Optional<ButtonType> alerta = alert.showAndWait();
        if(alerta.get() == ButtonType.OK){
            cafe.remove(c);
            //tvCafe.setItems((ObservableList)pedidos1);
            txtsaldoActual.setText(Double.parseDouble(txtsaldoActual.getText())  + c.getPrecioT() + "");
        }
        else{
            
        }
         }
        
        
    }
    
    @FXML
    void recargarSaldo(ActionEvent event) {
        
            try{
                btnPedir.setDisable(false);
                btnModificar.setDisable(false);
                btnBorrar.setDisable(false);
                btnfiltrar.setDisable(false);
                btnPedir.setStyle("-fx-background-color: #FF8633;");
                btnBorrar.setStyle("-fx-background-color: #FF3342;");
                btnModificar.setStyle("-fx-background-color: #77FF33;");

                if(Double.parseDouble(tfSaldo.getText())>0){
                if(Double.parseDouble(tfSaldo.getText())>0){
                double saldoactual1 = Double.parseDouble(tfSaldo.getText());
                txtsaldoActual.setText(Double.parseDouble(txtsaldoActual.getText())+ saldoactual1 + "");
            }
                else{
                    txtsaldoActual.setText("0");
                }
                    
                }
                else{
                
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("NUMERO NO DEBE DE SER NEGATIVO");
            alert.showAndWait();
                }
                    
                    
                
               
                
                
            }catch(NumberFormatException e){
                btnPedir.setDisable(true);
                btnModificar.setDisable(true);
                btnBorrar.setDisable(true);
                btnfiltrar.setDisable(true);
                
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("DEBE DE INGRESAR UN NUMERO - NO UNA LETRA");
            alert.showAndWait();
            }
    }
    
    @FXML
    void agregarPedido(ActionEvent event) {
        
        try{
        
        btnModificar.setDisable(false);
        btnBorrar.setDisable(false);
        btnfiltrar.setDisable(false);
        
        double saldoActual=Double.parseDouble(txtsaldoActual.getText());
        double resultado = (saldoActual - (precioT()));
        
        if(Double.parseDouble(txtsaldoActual.getText())<=0 || (precioT()> saldoActual)){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("DEBE DE RECARGAR PRIMERO");
            alert.showAndWait();
            
        }
        else{
            double cantidad = Double.parseDouble(tfCantidad.getText());
            Cafe p=new Cafe(nombreBotones(),cbTipo.getValue(),precioCafe(),cantidad,precioT());
            this.cafe.add(p);
            tvCafe.setItems(cafe);
            txtsaldoActual.setText(saldoActual - (precioT()) + "" );
            
        }
        
        }catch(NumberFormatException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("INGRESAR LOS DATOS PRIMERO");
            alert.setContentText("EL DATO DEBE DE SER UN NUMERO");
            alert.showAndWait();
            
        }
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        txtsaldoActual.setText("0");
        btnPedir.setDisable(true);
        btnModificar.setDisable(true);
        btnBorrar.setDisable(true);
        btnfiltrar.setDisable(true);
        btnfiltrar.setDisable(true);
        cbTipo.setItems(FXCollections.observableArrayList("Pequeño" , "Mediano" , "Grande"));
        cafe = FXCollections.observableArrayList();      
        this.colNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
        this.colTipo.setCellValueFactory(new PropertyValueFactory("tipo"));
        this.colPrecio.setCellValueFactory(new PropertyValueFactory("precio"));
        this.colCantidad.setCellValueFactory(new PropertyValueFactory("cantidad"));
        this.colPrecioTotal.setCellValueFactory(new PropertyValueFactory("precioT"));
    }
    
   
    public String nombreBotones(){
        
        if(rb1.isSelected()){
            return "Capuccino";
        }
        else if(rb2.isSelected()){
            return "Cafe Latte";
        }
        else{
            return "Cortado";
        }
        
    }
    
    public double precioT(){
        double precioTd;
        
        if(cbTipo.getValue()== "Pequeño"){
            precioTd = Double.parseDouble(tfCantidad.getText()) * 1.0;
            return precioTd;
        }
        else if(cbTipo.getValue()== "Mediano"){
            precioTd = Double.parseDouble(tfCantidad.getText()) * 2.0;
            return precioTd;
        }
        else {
            precioTd = Double.parseDouble(tfCantidad.getText()) * 3.0;
            return precioTd;
        }
            
        
    }
    
     public double precioCafe(){
         if(cbTipo.getValue()== "Pequeño")
            return  1.0 ;
         else if (cbTipo.getValue()== "Mediano"){
             return 2.0;
         }
         else{
             return 3.0;
         }
     }
     

}