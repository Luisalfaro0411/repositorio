package Monje;
/*PROGRAMACIÓN MULTIHILO: CONFESIONARIO
El problema consiste en una parroquia donde hay un único 
cura que sólo tiene un único confesionario y varias sillas 
para esperar. Cuando no hay creyentes, el cura se sienta en una 
silla y se duerme. Cuando llega un nuevo creyente, éste o bien 
despierta al cura o —si el cura está confesando a otro creyente— 
se sienta en una silla. El problema consiste en realizar la actividad 
del confesionario sin que ocurran condiciones de carrera.*/

public class Main {

	public static void main(String[] args) {
		Mesa mesa= new Mesa(5);
		for(int i = 1 ; i<=5 ; i++) {
			Monje m = new Monje(mesa,i);
			m.start();
		}
	}

}
