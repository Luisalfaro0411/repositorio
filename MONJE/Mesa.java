package Monje;

public class Mesa {
	
	private boolean[] monjes;

	public Mesa(int numMonjes) {
		this.monjes=new boolean[numMonjes];
	}
	
	public int monjeIzquierda(int numero) {
		return numero;
	}
	
	public int monjeDerecho(int numero) {
		if(numero == 0) {
			return this.monjes.length-1;
		}
		else {
			return numero-1;
		}
	}
	
	public synchronized void comerMonje(int indice) {
		while(monjes[monjeIzquierda(indice)] || monjes[monjeDerecho(indice)]) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		monjes[monjeIzquierda(indice)]=true;
		monjes[monjeDerecho(indice)]=true;
	}
	
	public synchronized void nocomerMonje(int indice) {
		monjes[monjeIzquierda(indice)]=false;
		monjes[monjeDerecho(indice)]=false;
		notifyAll();
	}
	
	
	
	
		
	}
	

