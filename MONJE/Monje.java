package Monje;

public class Monje extends Thread{
	private Mesa mesa;
	private int comensal;
	private int indice;
	public Monje(Mesa mesa, int comensal) {
		super();
		this.mesa = mesa;
		this.comensal = comensal;
		this.indice=comensal-1;
	}
	@Override
	public void run() {
		
		this.rezando();
		mesa.comerMonje(indice);
		this.comiendo();
		System.out.println("El monje "  + comensal + " deja de comer , los tenedores estan libres: " + (mesa.monjeIzquierda(indice)+1)+ " " + (mesa.monjeDerecho(indice) + 1));
		mesa.nocomerMonje(indice);
	}
	
	public void rezando() {
		System.out.println("Monje " + comensal + " esta rezando");
		try {
			sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void comiendo() {
		System.out.println("Monje " + comensal + " esta comiendo");
		try {
			sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
}
