import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Ruleta extends Thread{
	public static double dinero;
	protected ArrayList<Jugador> jugadores;
	
	public Ruleta (ArrayList<Jugador> jugadores) {
		this.dinero = 3000;
		this.jugadores = jugadores;
	}
	
	public void run() {
		System.out.println("EL JUEGO EMPIEZA");
		System.out.println("LA RULETA TIENE 3000 EUROS");
		System.out.println("EL JUGADOR 1 EMPIEZA CON 300 EUROS");
		System.out.println("EL JUGADOR 2 EMPIEZA CON 300 EUROS");
		System.out.println("EL JUGADOR 3 EMPIEZA CON 300 EUROS");
		System.out.println("EL JUGADOR 4 EMPIEZA CON 300 EUROS");
		
		Random num = new Random();
		
		int apuesta;
		
		while(jugadores.size() > 0) {
			int correcto = num.nextInt(36)+1;
			int total=3000;
			int pierde=360;
			int gana=10;
			System.out.println("DINERO BANCA: " + getDinero());
			System.out.println("=======================================");
			System.out.println("La ruleta ha sacado el numero: " + correcto);
			for (int pos=0; pos < jugadores.size(); pos++) {
				apuesta = num.nextInt(36) + 1;
				System.out.println("El jugador " + (pos+1) + " elige el numero: " + apuesta);
				if(apuesta == correcto) {
					jugGanar(pos);
					
					
				} else {
				
					jugPierde(pos);
					
				}
				try {
					TimeUnit.MILLISECONDS.sleep(100);
				} catch (InterruptedException e) {
					System.out.println("????????????");
					e.printStackTrace();
				}
			}
			try {
				TimeUnit.MILLISECONDS.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("????????????");
				e.printStackTrace();
			}
				
			}
				
		
		System.out.println("NADIE TIENE DINERO , TODOS QUEDAN ELIMINADOS");
	}
	
	public double getDinero() {
		return dinero;
	}
	
	
	public void jugGanar(int num) {
		this.dinero = this.dinero - 36;
		jugadores.get(num).dinero=jugadores.get(num).dinero+360;
		System.out.println( "El jugador " + jugadores.get(num).num + " ha ganado 360€. " + "\nTiene " + jugadores.get(num).dinero);
		
	}
	
	public void jugPierde(int num) {
		this.dinero = this.dinero + 10;
		jugadores.get(num).dinero=jugadores.get(num).dinero-10;
		if (jugadores.get(num).dinero <= 0) {
			
			System.out.println("El jugador " + (jugadores.get(num).num) + " ha sido eliminado por falta de dinero");
			jugadores.remove(num);
		} else {
			System.out.println("El jugador " + jugadores.get(num).num + " pierde 10€\nEl jugador " + jugadores.get(num).num + " tiene " + jugadores.get(num).dinero +"€");
			System.out.println("");
			
		}
	}
	
}
