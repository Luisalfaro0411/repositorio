
public class Jugador {
	public static int numeros = 0;
    public int num;
    public double dinero;
	
    public Jugador() {
        numeros++;
        num = numeros;
        this.dinero = 300;
    }
    
    public int getNum() {
    	return num;
    	}
    
    public void setNum(int num) {
    	this.num = num;
    	}
    
    public double getDinero() {
    	return dinero;
    	}
    public void setDinero(double dinero) {
    	this.dinero = dinero;
    	}
	
	
}
