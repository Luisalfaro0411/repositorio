package PracticaBBDD;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
	private Connection connection;
	private String url;
	private String usuario;
	private String contraseña;
	
	public Conexion(String url, String usuario, String contraseña) {
		super();
		this.url = url;
		this.usuario = usuario;
		this.contraseña = contraseña;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	public void establecerConexion() throws SQLException, ClassNotFoundException {
		
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(url,usuario,contraseña);
			
	}
	public void cerrarConexion() throws SQLException {
		connection.close();
	}
	
}
