package PracticaBBDD;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class metodos {
	
	public static void menu() throws ClassNotFoundException, SQLException {
		System.out.println("MENU DE BBDD");
    	System.out.println("");
    	System.out.println("1. INSERTAR DATOS A LA TABLA FABRICANTE");
    	System.out.println("2. INSERTAR DATOS A LA TABLA PRODUCTOS");
    	System.out.println("3. LEER DATOS DE FABRICANTE ASUS");
    	System.out.println("4. LEER LOS 5 PRODUCTOS MAS BARATOS");
    	System.out.println("5. EDITAR PRODUCTOS CON ID 1");
    	System.out.println("6. ELIMINAR PRODUCTO NUMERO 2");
    	    	
	}
	
	public static void insertarFabricante(String nombre,Connection connection) throws ClassNotFoundException, SQLException {
		Scanner t=new Scanner(System.in);
		PreparedStatement stmt;
		String query= "insert into fabricante (nombre) values (?)" ;
		stmt = connection.prepareStatement(query);
		System.out.println("Ingresar el nombre");
		nombre=t.next();
		stmt.setString(1, nombre);
		stmt.executeUpdate();
		System.out.println("SE AGREGO");
		tablaFabricante();
		
	}
	
	public static void tablaFabricante() throws ClassNotFoundException, SQLException {
		Conexion con = new Conexion("jdbc:mysql://localhost:3306/tienda" , "root","Luisalfaro0411");
		con.establecerConexion();
		Connection connection = con.getConnection();
		PreparedStatement stmt;
		String query= "select * from fabricante";
		stmt = connection.prepareStatement(query);
		ResultSet rs=stmt.executeQuery();
		while(rs.next()) {
			int edad= rs.getInt(1);
			String nombre = rs.getString(2);
			System.out.println(edad + "||" + nombre);
		
		}
	}
	
	public static void insertarProductos(String nombre , double precio, int codigo_fabricante,Connection connection) throws ClassNotFoundException, SQLException {
		Scanner t=new Scanner(System.in);
		PreparedStatement stmt;
		String query= "insert into producto (nombre,precio,codigo_fabricante) values (?,?,?)";
		stmt = connection.prepareStatement(query);
		System.out.println("Ingresar el nombre del producto");
		nombre=t.next();
		System.out.println("Ingresar el precio del producto");
		precio=t.nextDouble();
		System.out.println("Ingresar el codigo del fabricante");
		codigo_fabricante=t.nextInt();
		stmt.setString(1, nombre);
		stmt.setDouble(2, precio);
		stmt.setInt(3, codigo_fabricante);
		stmt.executeUpdate();
		System.out.println("SE AGREGO");
		tablaProducto();
		
	}
	
	public static void tablaProducto() throws ClassNotFoundException, SQLException {
		Conexion con = new Conexion("jdbc:mysql://localhost:3306/tienda" , "root","Luisalfaro0411");
		con.establecerConexion();
		Connection connection = con.getConnection();
		PreparedStatement stmt;
		String query= "select * from producto";
		stmt = connection.prepareStatement(query);
		ResultSet rs=stmt.executeQuery();
		while(rs.next()) {
			int edad= rs.getInt(1);
			String nombre = rs.getString(2);
			double precio= rs.getDouble(3);
			int codigo2 = rs.getInt(4);
			System.out.println(edad + "||" + nombre + "||" + precio + "||" + codigo2);
		
		}
	}
	
	public static void leerAsus(Connection connection) throws ClassNotFoundException, SQLException {
		Scanner t=new Scanner(System.in);
		PreparedStatement stmt;
		System.out.println("Ingresar el codigo del fabricante para buscar");
		int codigo=t.nextInt();
		String query= "select * from producto where codigo_fabricante = " + codigo;
		stmt = connection.prepareStatement(query);
		ResultSet rs=stmt.executeQuery();
		while(rs.next()) {
			int edad= rs.getInt(1);
			String nombre = rs.getString(2);
			double precio= rs.getDouble(3);
			int codigo2 = rs.getInt(4);
			System.out.println(edad + "||" + nombre + "||" + precio + "||" + codigo2);
		
		}
	}
	
	public static void eliminarProducto(int codigo,Connection connection) throws ClassNotFoundException, SQLException {
		Scanner t=new Scanner(System.in);
		PreparedStatement stmt;
		String query= "delete from producto where codigo = ?";
		stmt = connection.prepareStatement(query);
		System.out.println("Ingresar el codigo para eliminar");
		codigo=t.nextInt();
		stmt.setInt(1, codigo);
		stmt.executeUpdate();
		System.out.println("SE ELIMINO");
		tablaProducto();
	}
	
	public static void editarProducto(String nombre , double precio, int codigo_fabricante,Connection connection) throws ClassNotFoundException, SQLException {
		Scanner t=new Scanner(System.in);
		int codigo;
		System.out.println("Ingresar el codigo para editar");
		codigo=t.nextInt();
		PreparedStatement stmt;
		String query= "UPDATE producto SET nombre=? , precio=?, codigo_fabricante=? WHERE codigo= " + codigo;
		stmt = connection.prepareStatement(query);
		
		//------------------------
		
		System.out.println("Ingresar el nombre del producto a cambiar");
		nombre=t.next();
		System.out.println("Ingresar el precio del producto a cambiar");
		precio=t.nextDouble();
		System.out.println("Ingresar el codigo del fabricante a cambiar");
		codigo_fabricante=t.nextInt();
		stmt.setString(1, nombre);
		stmt.setDouble(2, precio);
		stmt.setInt(3, codigo_fabricante);
		stmt.executeUpdate();
		if(stmt.executeUpdate()>0) {
			System.out.println("SE MODIFICO");
			tablaProducto();
		}
		
	}
	
	public static void leerBarato(Connection connection) throws ClassNotFoundException, SQLException {
		PreparedStatement stmt;
		String query= "SELECT * FROM producto  order by precio LIMIT 5 ";
		stmt = connection.prepareStatement(query);
		ResultSet rs=stmt.executeQuery();
		while(rs.next()) {
			int edad= rs.getInt(1);
			String nombre = rs.getString(2);
			double precio= rs.getDouble(3);
			int codigo2 = rs.getInt(4);
			System.out.println(edad + "||" + nombre + "||" + precio + "||" + codigo2);
		
		}
	}
	
	
}
